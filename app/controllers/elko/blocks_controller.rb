class Elko::BlocksController < ElkoController
  helper_method :block_model
  before_action :set_item_name, only: [:show, :index, :new, :edit]

  def index
    @items = block_model.all
  end

  def new
    @item = block_model.new
  end

  def create
    @item = block_model.new block_params

    if @item.save
      flash[:success] = "Successfully created #{block_model.to_s}"
      redirect_to action: "show", id: @item
    else
      flash[:danger] = @item.errors.full_messages.to_sentence
      redirect_to action: "index", controller: block_model.to_s.pluralize.underscore
    end
  end

  def edit
    @item = block_model.friendly.find(params[:id])
  end

  def update
    @item = block_model.friendly.find(params[:id])

    if @item.update block_params
      flash[:success] = "Successfully updated #{block_model.to_s}"
      redirect_to action: "show", id: @item
    else
      flash[:danger] = @item.errors.full_messages.to_sentence
      redirect_to action: "index", controller: block_model.to_s.pluralize.underscore
    end
  end

  def show
    @item = block_model.friendly.find(params[:id])
  end

  def destroy
    @item = block_model.friendly.find(params[:id])

    if @item.destroy
      flash[:success] = "Successfully deleted #{block_model.to_s}"
      redirect_to action: "index", controller: block_model.to_s.pluralize.underscore
    else
      flash[:danger] = @item.errors.full_messages.to_sentence
      redirect_to action: "index", controller: block_model.to_s.pluralize.underscore
    end
  end

  def toggle_status
    @item = block_model.friendly.find(params[:id])
    @item.toggle_status
    redirect_back(fallback_location: elko_root_path)
  end

  private
  def block_params 
    params.require("#{block_model.to_s.underscore}").permit!
  end

  def set_item_name
    @item_name = block_model.to_s
  end
end