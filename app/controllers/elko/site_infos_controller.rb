class Elko::SiteInfosController < ElkoController

  def edit
    @site_info = Elko::SiteInfo.first
  end

  def update
    @site_info = Elko::SiteInfo.first

    if @site_info.update site_info_params
      flash[:success] = 'Successfully updated your website information'
      redirect_to elko_root_path
    else
      flash[:danger] = @site_info.errors.full_messages.to_sentence
      render 'edit'
    end 
  end

  private
    def site_info_params
      params.require(:elko_site_info).permit!
    end

end