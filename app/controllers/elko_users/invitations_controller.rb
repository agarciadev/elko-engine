class ElkoUsers::InvitationsController < Devise::InvitationsController

  layout :set_layout

  def edit
    super
  end

  def after_invite_path_for(resource)
    elko_users_path
  end

  def after_accept_path_for(resource)
    elko_root_path
  end

  private

  def set_layout
    if action_name == 'edit' 
      'elko/auth'
    else 
      'elko/application'
    end
  end

end