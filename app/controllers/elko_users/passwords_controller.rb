class ElkoUsers::PasswordsController < Devise::PasswordsController
  layout 'elko/auth'

  def after_sending_reset_password_instructions_path_for(resource_name)
    new_elko_user_session
  end

end