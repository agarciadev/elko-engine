class ElkoUsers::RegistrationsController < Devise::RegistrationsController
  before_action :configure_permitted_parameters

  def new
    flash[:danger] = 'You are unauthorized to perform that action'
    redirect_to root_path # root path will exist when a root application exists
  end

  def create
    flash[:danger] = 'You are unauthorized to perform that action'
    redirect_to root_path # root path will exist when a root application exists
  end

  def after_update_path_for(resource)
    elko_users_path
  end

  private
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name])
  end

end