class ElkoUsersController < ElkoController

  def index
    @users = ElkoUser.all
  end

  def new
    @user = ElkoUser.new
  end

  def create
    @user = ElkoUser.new user_params
    @user.password = 'password'

    if @user.save
      flash[:success] = "Successfully added user to the Elko Engine"
      redirect_to elko_users_path
    else
      flash[:danger] = @user.errors.full_messages.to_sentence
      render :new
    end
  end

  def destroy
    @user = ElkoUser.find(params[:id])

    if @user.destroy
      flash[:success] = "Successfully deleted User"
      redirect_to elko_users_path
    else
      flash[:danger] = @user.errors.full_messages.to_sentence
      redirect_to elko_users_path
    end
  end

  private
  def user_params
    params.require(:elko_user).permit(:email, :first_name, :last_name, :password, :password_confirmation)
  end 

end