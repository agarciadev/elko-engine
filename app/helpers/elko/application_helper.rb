module Elko
  module ApplicationHelper

    def insert_images(item, text)
      images = text.scan(/\[ Image \d+ [^.!?\s][^.!?]*(?:[.!?](?!['"]?\s|$)[^.!?]*)*[.!?]?['"]?(?=\s|$) \]/)
      modifedText = text
      newText = ''

      return text if images.count < 1

      images.each do |image| 
        id = image.match(/\d+/)
        alt = image.match(/(?<=')(.+)(?=')/)
        path = ''
        if (item.images.exists?(id.to_s))
          path = rails_blob_url(item.images.find(id.to_s))
        else 
          path = 'none'
        end
        modifedText = modifedText.gsub("[ Image #{id.to_s} '#{alt.to_s}' ]", "<div class='image-container'><img alt='#{ alt }' width='500' src='#{ path }'></img></div>")
        newText = modifedText
      end
      newText
    end

    def format(item, text)
      newText = insert_images(item, text)
      sanitize newText.gsub('h1', 'h2')
    end

  end
end
