module Elko
  class SiteInfo < ApplicationRecord
  
    has_one_attached :logo

  end
end