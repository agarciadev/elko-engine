module Elko
  class UserBlock < ApplicationRecord
    belongs_to :user
    belongs_to :block
  end
end
