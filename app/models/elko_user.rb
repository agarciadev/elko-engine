class ElkoUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :validatable,
          :trackable, :invitable

  validates_presence_of :email
  validate :password_not_needed, on: :create

  def password_not_needed

  end

  def fullname
    if self.first_name.present?
      if  self.last_name.present?
        "#{first_name} #{last_name}"
      else
        "#{first_name}"
      end
    else
      "#{email}"
    end
  end
end
