Rails.application.routes.draw do

  devise_for :elko_users, {
    controllers: {
      sessions:      'elko_users/sessions',
      registrations: 'elko_users/registrations',
      invitations:   'elko_users/invitations',
      passwords:     'elko_users/passwords'
    },
    module: :devise
  }

  resources :elko_users, except: [:edit, :update] 
  namespace :elko do
    
    resource  :site_info, only: [:edit, :update]
    root 'dashboard#index'
    get '/settings', to: 'dashboard#settings', as: 'settings'
  end
end
