class CreateElkoSiteInfo < ActiveRecord::Migration[5.2]
  def change
    create_table :site_infos do |t|
      
      t.string :website_name

      t.timestamps
    end

    Elko::SiteInfo.create
  end
end
