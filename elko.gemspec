$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "elko/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "elko"
  s.version     = Elko::VERSION
  s.authors     = ["Angel Garcia"]
  s.email       = ["angelgarcia@agarciadev.com"]
  s.homepage    = ""
  s.summary     = ""
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.2.1"

  s.add_dependency 'pg'
  s.add_dependency 'bootsnap'
  s.add_dependency 'devise', '~> 4.2'
  s.add_dependency 'devise_invitable', '~> 1.7.0'
  s.add_dependency 'simple_form'
  s.add_dependency 'font-awesome-rails', '~> 4.7.0.4'
  s.add_dependency 'jquery-rails', '~> 4.3.3'
  s.add_dependency 'sass-rails'
  s.add_dependency 'uglifier'
  # s.add_dependency 'bootstrap', '>= 4.3.1'
  s.add_dependency 'trix'
  s.add_dependency 'mini_magick'
  s.add_dependency 'friendly_id', '~> 5.2.4'

  # DEVELOPMENT
  s.add_development_dependency 'letter_opener'
  s.add_development_dependency 'better_errors'
  s.add_development_dependency 'binding_of_caller'
  s.add_development_dependency 'byebug'
end
