class BlockGenerator < Rails::Generators::Base
  include Rails::Generators::Migration
  source_root File.expand_path('templates', __dir__)
  argument :model, type: :string
  argument :app_mvc, type: :string, optional: true
  argument :required_module, type: :string, optional: true

  def edit_routes
    route("\tresources :#{model.pluralize.underscore} do
      member do
        post :toggle_status
      end
    end")
  end

  def generate_model
    generate_block_model
  end

  def self.next_migration_number(dir)
    Time.now.utc.strftime("%Y%m%d%H%M%S")
  end

  private
  def route(routing_code)
    log 'route', routing_code
    sentinel = 'namespace :elko do'
  
    in_root do
      gsub_file 'config/routes.rb', /(#{Regexp.escape(sentinel)})/mi do |match|
        "#{match}\n  #{routing_code}\n"
      end
    end
  end

  def generate_block_model
    migration_template "block_migration.rb",             "db/migrate/create_#{model.pluralize.underscore}.rb"
    template 'block_model_controller_template.template', "app/controllers/elko/#{model.pluralize.underscore}_controller.rb"
    template 'block_model_template.template',            "app/models/#{model.singularize.underscore}.rb"
    template 'block_model_view_template.template',       "app/views/elko/#{model.pluralize.underscore}/_form.html.erb"
    template 'block_model_view_template.template',       "app/views/elko/#{model.pluralize.underscore}/_content.html.erb"

    if app_mvc == 'app_mvc'
      path = required_module.nil? ? "#{model.pluralize.underscore}" : "#{required_module.underscore}/#{model.pluralize.underscore}"
      template 'block_model_view_template.template',       "app/views/#{path}/index.html.erb"
      template 'block_model_view_template.template',       "app/views/#{path}/show.html.erb"
      template 'block_model_view_template.template',       "app/views/#{path}/new.html.erb"
      template 'block_model_view_template.template',       "app/views/#{path}/edit.html.erb"
      template 'block_model_view_template.template',       "app/views/#{path}/_form.html.erb"
      template 'controller_template.template',             "app/controllers/#{path}_controller.rb"
    end
  end
end
