class Create<%= model.pluralize.underscore.camelize %> < ActiveRecord::Migration[5.2]
  def up
    create_table <%= ":#{model.pluralize.underscore}" %> do |t|

      t.string :name
      
      t.string :slug, unique: :true
      t.integer :status, default: 0
      t.timestamps
    end

    Elko::Block.create(title: "<%= model.pluralize.titleize %>", controller: "<%= model.pluralize.underscore %>")
  end

  def down
    Elko::Block.find_by_title("<%= model.pluralize.titleize %>").destroy
    drop_table <%= ":#{model.pluralize.underscore}" %>
  end
end