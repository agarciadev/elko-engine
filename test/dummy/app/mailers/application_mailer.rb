class ApplicationMailer < ActionMailer::Base
  default from: 'client@example.com'
  layout 'mailer'
end
